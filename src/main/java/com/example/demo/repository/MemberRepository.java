package com.example.demo.repository;

import com.example.demo.model.Member;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MemberRepository extends CrudRepository<Member, String> {
    Member findByNameIgnoreCase(String name);
    List<Member> findByNameLike(String name);

    @Override
    Optional<Member> findById(String s);

    Member findByNameAndEmail(String name, String email);
}
