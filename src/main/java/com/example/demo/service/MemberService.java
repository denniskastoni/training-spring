package com.example.demo.service;

import com.example.demo.model.Member;

import java.util.List;

public interface MemberService {

    Member findByName(String name);

    Member findById(String id);

    List<Member> findByNameLike(String name);

    Member save(Member member);

    void delete(Member member);
}
