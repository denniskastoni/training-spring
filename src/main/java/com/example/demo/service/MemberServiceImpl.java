package com.example.demo.service;

import com.example.demo.model.Member;
import com.example.demo.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MemberServiceImpl implements MemberService {

    @Autowired
    private MemberRepository memberRepository;

    @Override
    public Member findByName(String name) {
        return memberRepository.findByNameIgnoreCase(name);
    }

    @Override
    public Member findById(String id) {
        return memberRepository.findById(id).get();
    }

    @Override
    public List<Member> findByNameLike(String name) {
        return memberRepository.findByNameLike(name);
    }

    @Override
    public Member save(Member member) {
        return memberRepository.save(member);
    }

    @Override
    public void delete(Member member) {
        memberRepository.delete(member);
    }
}
