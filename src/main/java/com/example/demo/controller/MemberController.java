package com.example.demo.controller;

import com.example.demo.model.Member;
import com.example.demo.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class MemberController {
    @Autowired
    private MemberService memberService;

    @GetMapping(value = "/members/{id}")
    public String findPeopleName(@PathVariable String id){
        Member member = memberService.findByName(id);
        return member == null ? "tidak ada nama " + id + " di database" : member.toString();
    }

    @DeleteMapping(value = "/delete/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public String deletePeople(@PathVariable String id){
        try {
            Member member = memberService.findById(id);
            memberService.delete(member);

            return "success delete";
        }catch (Exception e){
            e.printStackTrace();
            return "fail delete";
        }
    }

    @PutMapping(value = "/members/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public String updatePeople(@PathVariable String id, @RequestBody Member member){
        Member result = memberService.save(member);
        if(result == null){
            return "gagal update ke " + member.toString();
        }
        else
            return "berhasil update ke " + member.toString();
    }

    @PostMapping(value = "/members", consumes = MediaType.APPLICATION_JSON_VALUE)
    public String createPeople(@RequestBody Member member){
        Member result = memberService.save(member);
        if(result == null){
            return "gagal menyimpan data";
        }
        else return "sukses menyimpan data";
    }



}
